define [
	'HelloWorldView', 
	'style!css/app.css'
], (HelloWorldView) ->

	$(document).ready ->
		helloWorldView = new HelloWorldView
		$('#app').html helloWorldView.render().$el