define [
	'text!templates/hello-world.html', 
	'less/hello-world.less'
], (helloWorldTemplate) ->

	class HelloWorldView extends Backbone.View
		tagName: 'h1'
		template: _.template helloWorldTemplate
		render: ->
			@$el.html @template { content: 'Hello World' }
			@