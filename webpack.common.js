var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: {
		app: './src/coffee/app.coffee',
		vendor: [
			"jquery",
			"backbone",
			"underscore"
		]
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	module: {
		rules: [
			{ test: /\.coffee$/, loader: "coffee-loader" },
			{ test: /\.png$/, loader: "url-loader?mimetype=image/png" },
			{ test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
			{ test: /\.(jpg|gif|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
			{ test: /\.css$/, loader: 'css-loader' },
			{ test: /\.less$/, use: [{ loader: "style-loader" }, { loader: "css-loader" }, { loader: "less-loader" }] }
		]
	},
	resolveLoader: {
		alias: { // These Aliases are to maintain compatibility with projects that use requireJS loaders.
			style: 'style-loader', // https://github.com/webpack-contrib/css-loader
			text: 'html-loader' // https://github.com/webpack-contrib/html-loader
		}
	},
	resolve: {
		modules: [
		  path.join(__dirname, 'src/coffee/'), // baseUrl
		  path.join(__dirname, 'node_modules')
		],
		extensions: [
			'.js', '.coffee'
		],
		alias: {
			templates: path.resolve(__dirname, 'src/templates/'),
			images: path.resolve(__dirname, 'src/images/'),
			less: path.resolve(__dirname, 'src/less/'),
			css: path.resolve(__dirname, 'src/css/')
		}
	},
	plugins: [
		new webpack.ProvidePlugin({
			$       : "jquery",
			jQuery  : "jquery",
			Backbone: "backbone",
			_       : "underscore"
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: "vendor",
			filename: "vendor.bundle.js"
		})
	]
};