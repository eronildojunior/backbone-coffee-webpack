## SPA with BackboneJS and Webpack

This project is a boilerplate with webpack and some loaders to start a developing a modular SPA using BackboneJS and CoffeeScript.

Run the project in develop:

```sh
npm start
```

Build the project to production:

```sh
npm run build
```

**Note: You can import a .less file in your CoffeeScript file, case you need another css processor just install the plugin package and add in webpack.common.js**

Main technologies used in this project:

- [Webpack](https://webpack.js.org/)
- [BackboneJS](http://backbonejs.org/)
- [Underscore](http://underscorejs.org/)
- [JQuery](https://jquery.com/)
- [CoffeeScript](http://coffeescript.org/)
- [RequireJS](http://requirejs.org/)
- [Less](http://lesscss.org/)